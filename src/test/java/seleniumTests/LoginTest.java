/*
package seleniumTests;


import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginTest extends BaseForTests{

    @Test
    void successfulLogin()  {
        login("Chef", "mdp");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
    }

    @Test
    void passwordError(){
        login("Chef", "erreur");
        WebElement  error= driver.findElement(By.className("alert-danger"));
        String errorText=read(error);
        assertTrue(errorText.contains("Bad credentials"));
    }

    @Test
    void ajoutProfparProfError(){
        login("Lovelace", "lovelace");
        WebElement listProfs = driver.findElement(By.id("listTeacher"));
        listProfs.click();
        String page = driver.getPageSource();

        WebElement addTeacher = driver.findElement(By.id("addTeacher"));
        addTeacher.click();
        String titre = driver.getTitle();
        assertEquals("Error", titre);
        driver.get(serverAdress + "/listTeachers");
        assertEquals(page, driver.getPageSource());
    }

    @Test
    void ajoutProf() throws IOException {
        login("Chef", "mdp");

        WebElement listProfs = driver.findElement(By.id("listTeacher"));
        listProfs.click();

        WebElement addTeacher = driver.findElement(By.id("addTeacher"));
        addTeacher.click();

        WebElement firstNameField = driver.findElement(By.id("firstName"));
        WebElement lastNameField = driver.findElement(By.id("lastName"));
        WebElement createButton = driver.findElement(By.cssSelector("[type=submit]"));

        write(firstNameField, "TeacherfirstnameforTest");
        write(lastNameField, "TeacherLastNameForTest");
        click(createButton);

        assertEquals("Teacher List", driver.getTitle());

        assertTrue(driver.getPageSource().contains("TeacherLastNameForTest"));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("~/tmp/a.png"));
    }

}
*/