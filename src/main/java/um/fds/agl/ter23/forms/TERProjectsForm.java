package um.fds.agl.ter23.forms;
import java.util.ArrayList;

public class TERProjectsForm {
    private long id;
    private String teacherName;
    private String subject;
    private String secondTeacherName;
    private String listeEtu;
    
    public TERProjectsForm(long id, String teacherName, String subject, String secondTeacherName) {
        this.teacherName = teacherName;
        this.secondTeacherName = secondTeacherName;
        this.subject = subject;
        this.id = id;
    }

    public TERProjectsForm() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSecondTeacherName() {
        return secondTeacherName;
    }

    public void setSecondTeacherName(String secondTeacherName) {
        this.secondTeacherName = secondTeacherName;}



    public String getListeEtu() {
        return listeEtu;
    }

    public void addListeEtu (String nomEtu){
        this.listeEtu=nomEtu;
    }
}
