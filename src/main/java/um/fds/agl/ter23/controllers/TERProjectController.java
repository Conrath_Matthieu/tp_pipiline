package um.fds.agl.ter23.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter23.entities.TERProject;
import um.fds.agl.ter23.forms.TERProjectsForm;
import um.fds.agl.ter23.services.TERProjectService;

@Controller
public class TERProjectController {

    @Autowired
    private TERProjectService terProjectService;

    @GetMapping("/listTERProjects")
    public Iterable<TERProject> getTERProjects(Model model) {
        Iterable<TERProject> projects= terProjectService.getProjects();
        model.addAttribute("projects", projects);
        return projects;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = { "/addTERProjects" })
    public String showAddProjectPage(Model model) {
        TERProjectsForm terProjectsForm = new TERProjectsForm();
        model.addAttribute("projectForm", terProjectsForm);

        return "addTERProjects";
    }

    @PostMapping(value = { "/addTERProjects"})
    public String addTERProjects(Model model, @ModelAttribute("TERProjectsForm") TERProjectsForm terProjectsForm) {
        TERProject terProject;
        if(terProjectService.findById(terProjectsForm.getId()).isPresent()){
            terProject = terProjectService.findById(terProjectsForm.getId()).get();
            terProject.setTeacherName(terProjectsForm.getTeacherName());
            terProject.setSubject(terProjectsForm.getSubject());
            terProject.setSecondTeacherName(terProjectsForm.getSecondTeacherName());
        } else {
            terProject = new TERProject(terProjectsForm.getTeacherName(), terProjectsForm.getSubject(), terProjectsForm.getSecondTeacherName());
            terProject.setListeEtu(terProjectsForm.getListeEtu());
        }
        terProjectService.saveTerProject(terProject);
        return "redirect:/listTERProjects";

    }
}
