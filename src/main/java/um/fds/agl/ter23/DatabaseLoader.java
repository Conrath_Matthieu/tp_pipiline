package um.fds.agl.ter23;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import um.fds.agl.ter23.entities.Student;
import um.fds.agl.ter23.entities.TERManager;
import um.fds.agl.ter23.entities.TERProject;
import um.fds.agl.ter23.entities.Teacher;
import um.fds.agl.ter23.repositories.StudentRepository;
import um.fds.agl.ter23.repositories.TERManagerRepository;
import um.fds.agl.ter23.repositories.TERProjectRepository;
import um.fds.agl.ter23.repositories.TeacherRepository;

@Component
public class DatabaseLoader implements CommandLineRunner {
    private final TeacherRepository teachers;
    private final TERManagerRepository managers;

    private final StudentRepository students;

    private final TERProjectRepository projects;


    @Autowired
    public DatabaseLoader(TeacherRepository teachers, TERManagerRepository managers, StudentRepository students, TERProjectRepository projects) {
        this.teachers = teachers;
        this.managers = managers;
        this.students = students;
        this.projects = projects;
    }

    @Override
    public void run(String... strings) throws Exception {
        String permName = "ROLE_TEACHER";
        TERManager terM1Manager=this.managers.save(new TERManager("Le","Chef", "mdp", "ROLE_MANAGER"));
       SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken("Chef", "bigre",
                        AuthorityUtils.createAuthorityList("ROLE_MANAGER"))); // the actual password is not needed here
        this.teachers.save(new Teacher("Ada", "Lovelace", "lovelace",terM1Manager, permName));
        this.teachers.save(new Teacher("Alan", "Turing", "turing",terM1Manager, permName));
        this.teachers.save(new Teacher("Leslie", "Lamport", "lamport",terM1Manager, permName));
        this.students.save(new Student("Gustave", "Flaubert"));
        this.students.save(new Student("Frédéric", "Chopin"));

        this.projects.save(new TERProject("TITREEEEEe", "WDHZNJ ?KFZHEN", "Deuxième prof"));

        SecurityContextHolder.clearContext();

    }
}
