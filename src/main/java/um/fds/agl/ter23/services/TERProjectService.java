package um.fds.agl.ter23.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter23.entities.TERProject;
import um.fds.agl.ter23.repositories.TERProjectRepository;

import java.util.Optional;

@Service
public class TERProjectService {
    @Autowired
    private TERProjectRepository terProjectRepository;

    public Iterable<TERProject> getProjects() {
        return terProjectRepository.findAll();
    }

    public TERProject saveTerProject(TERProject terProject) {
        return terProjectRepository.save(terProject);
    }

    public Optional<TERProject> findById(long id) {
        return terProjectRepository.findById(id);
    }
}
