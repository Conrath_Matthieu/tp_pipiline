package um.fds.agl.ter23.entities;
import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TERProject {
    private @Id @GeneratedValue Long id;
    private String subject;
    private String teacherName;
    private String secondTeacherName;

    private String listeEtu;

    public TERProject() {}

    public TERProject(String subject, String teacherName, String secondTeacherName) {
        this.subject = subject;
        this.teacherName = teacherName;
        this.secondTeacherName = secondTeacherName;
    }

    public Long getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public void setSecondTeacherName(String secondTeacherName) {
        this.secondTeacherName = secondTeacherName;
    }

    public String getSecondTeacherName() {
        return this.secondTeacherName;
    }

    public void setListeEtu(String listeEtu) {
        this.listeEtu = listeEtu;
    }

    public String getListeEtu() {
        return listeEtu;
    }
}
