package um.fds.agl.ter23.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.HandlerResultMatchers;
import um.fds.agl.ter23.entities.Teacher;
import um.fds.agl.ter23.services.TeacherService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeacherService teacherService;

    @Captor
    ArgumentCaptor<Teacher> teacherCaptor;

    /*
    @Test
    @WithMockUser(username="Chef", password="mdp", roles="MANAGER")
    void addTeacherGet() throws Exception {
        when(teacherService.findById(10)).thenReturn(Optional.ofNullable(null));

        MvcResult result = mvc.perform(post("/addTeacher").param("firstname", "prénom").param("lastname", "nom").param("id", "10"))
                .andExpect(status().is3xxRedirection()).andReturn();

        verify(teacherService, atLeastOnce()).saveTeacher(teacherCaptor.capture());

        Teacher capturedTeacher = teacherCaptor.getValue();
        assertEquals("nom", capturedTeacher.getLastName());
    }*/
}