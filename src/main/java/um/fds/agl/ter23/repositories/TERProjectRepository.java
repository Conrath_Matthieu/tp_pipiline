package um.fds.agl.ter23.repositories;

import org.springframework.data.repository.CrudRepository;
import um.fds.agl.ter23.entities.TERProject;

public interface TERProjectRepository extends CrudRepository<TERProject, Long> {
    @Override
    TERProject save(TERProject project);

    TERProject findBySubject(String subject);

    @Override
    Iterable<TERProject> findAll();
}
